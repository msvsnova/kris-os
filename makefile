#------------------------------------------------------------------------------
#
# SuperNova APPLICATION - OS - MAKEFILE
# By FIST Kresimir Mandic 29.03.1999.
#
#------------------------------------------------------------------------------
TD          = X:\os\ 
YTD		    = Y:\NewVer.RAR\os\ 
COMD        = .\lib\oscommon.fle 

#------------------------------------------------------------------------------
# Compiler options
#------------------------------------------------------------------------------
## CC          = start /w c:\snova\sncd\nova
CC          = start /w c:\vscd\bin\nova
CFLAGS      = -w -ereadfgl
#------------------------------------------------------------------------------

OSFILES     =         $(TD)os.fle       $(TD)os_dd.fle    $(TD)amgr.fle      \
	$(TD)invlist.fle  $(TD)lamgr.fle    $(TD)lart.fle     $(TD)os_start.fle  \
	$(TD)likvid.fle   $(TD)linvlist.fle $(TD)linvrek.fle  $(TD)linvrepo.fle  \
	$(TD)os_a_dd.fle  $(TD)lizvrknj.fle $(TD)llikvid.fle  $(TD)lobramgr.fle  \
	$(TD)lobrkont.fle $(TD)lobrlik.fle  $(TD)lobrlok.fle  $(TD)lobrramg.fle  \
	$(TD)lobrrkon.fle $(TD)lobrrlik.fle $(TD)lobrrlok.fle $(TD)lobrrorg.fle  \
	$(TD)lobruamg.fle $(TD)lobrukon.fle $(TD)lobrulok.fle $(TD)losamgr.fle   \
	$(TD)loskar.fle   $(TD)loskont.fle  $(TD)loslok.fle   $(TD)losorgs.fle   \
	$(TD)losppar.fle  $(TD)losramgr.fle $(TD)losrkont.fle $(TD)losrlok.fle   \
	$(TD)losrppar.fle $(TD)losuamgr.fle $(TD)losukar.fle  $(TD)losukont.fle  \
	$(TD)losuorgs.fle $(TD)losuppar.fle $(TD)lresk.fle    $(TD)lsikar.fle    \
	$(TD)lsikont.fle  $(TD)lsiorgs.fle  $(TD)lvrpr.fle    $(TD)l_visman.fle  \
	$(TD)manjak.fle   $(TD)os_obrac.fle $(TD)obrlik.fle   $(TD)osdbutil.fle  \
	$(TD)oskar.fle    $(TD)oskt.fle     $(TD)osos.fle     $(TD)osprom.fle    \
	$(TD)osulaz.fle   $(TD)izvrknj.fle  $(TD)pocsta.fle   $(TD)art.fle       \
	$(TD)resk.fle     $(TD)rinvlist.fle $(TD)rinvrek.fle  $(TD)rinvrepo.fle  \
	$(TD)toascii.fle  $(TD)upariv.fle   $(TD)visak.fle    $(TD)visman.fle    \
	$(TD)vismanpr.fle $(TD)vrpr.fle     $(TD)os_zim.fle   $(TD)os_flds.fle   \
	$(TD)os_pren.fle  $(TD)os_pren2.fle $(TD)ospasswd.fle $(TD)osdelvm.fle   \
	$(TD)visakupd.fle $(TD)losulok.fle  $(TD)oscorect.fle $(TD)oschorg.fle   \
	$(TD)loschorg.fle $(TD)predamor.fle $(TD)lpredamo.fle $(TD)losart.fle    \
	$(TD)losuart.fle  $(TD)losrart.fle  $(TD)lrbaramg.fle $(TD)lrbauamg.fle  \
	$(TD)lrbaukon.fle $(TD)lrbarkon.fle $(TD)lpredkon.fle $(TD)lpreamot.fle  \
	$(TD)lprekont.fle $(TD)osrt_dd.fle  $(TD)osrt.fle     $(TD)osrt_1.fle    \
	$(TD)invrlist.fle $(TD)ososadd.fle  $(TD)ramorek.fle  $(TD)ramorepo.fle  \
	$(TD)lchorgal.fle $(TD)osaktiv.fle  $(TD)losaktiv.fle $(TD)losloksi.fle \
	$(TD)laktival.fle $(TD)os_ponob.fle $(TD)losobj.fle   $(TD)os_prim.fle   \
	$(TD)losuobj.fle  $(TD)losrobj.fle  $(TD)lizknjob.fle $(TD)os_kalk.fle   \
    $(TD)os_prims.fle $(TD)os_sklad.fle $(TD)os_tab.fle   $(TD)os_kalks.fle  \
    $(TD)lizknorg.fle $(TD)losrevobr.fle $(TD)ispis_zbroja.fle $(TD)os_rev.fle \
    $(TD)osdbfupd.fle $(TD)os_rba_kalk.fle $(TD)los_rba_kalk.fle             \
    $(TD)os_mnajag.fle $(TD)os_mnajam.fle $(TD)os_mnajfak.fle $(TD)lchorg_svi.fle \
    $(TD)volkos2.fle $(TD)los_mnaj.fle $(TD)los_mnaj2.fle $(TD)los_mnajag.fle \
    $(TD)osmnprfak.fle $(TD)losmnrek.fle $(TD)losmnpojed.fle $(TD)los_mnzap.fle \
    $(TD)os_mnzap.fle $(TD)os_idx.fle    $(TD)os_nal.fle  $(TD)los_nal.fle \
    $(TD)ospredrac.fle $(TD)lrbapred.fle $(TD)lrbapred_mj.fle $(TD)amgr2.fle \
    $(TD)l_amor.fle $(TD)lamgr_arh.fle $(TD)osamgruparh.fle $(TD)ossredpov.fle

# COMMON  = amgr.ls os_dd.ls os_ddx.ls os_flds.ls osos.ls osulaz.ls resk.ls      


all     : $(OSFILES) $(COMMON)


amgr.ls:  amgr.lgc
    $(CC) $(CFLAGS) appl=amgr dotfle=$(COMD)
    @echo " ">amgr.ls

os_dd.ls:  os_dd.lgc
    $(CC) $(CFLAGS) appl=os_dd dotfle=.\lib\os_dd.fle 
    @echo " ">os_dd.ls

os_ddx.ls:  os_ddx.lgc
    $(CC) $(CFLAGS) appl=os_ddx dotfle=.\lib\os_dd.fle 
    @echo " ">os_ddx.ls

os_flds.ls:  os_flds.lgc
    $(CC) $(CFLAGS) appl=os_flds dotfle=.\lib\os_dd.fle 
    @echo " ">os_flds.ls

osos.ls:  osos.lgc
    $(CC) $(CFLAGS) appl=osos dotfle=$(COMD)
    @echo " ">osos.ls

osulaz.ls:  osulaz.lgc
    $(CC) $(CFLAGS) appl=osulaz dotfle=$(COMD)
    @echo " ">osulaz.ls

resk.ls:  resk.lgc
    $(CC) $(CFLAGS) appl=resk dotfle=$(COMD)
    @echo " ">resk.ls

###########################################33

$(TD)os_dd.fle:  os_dd.lgc
    $(CC) $(CFLAGS) appl=os_dd dotfle=$(TD)os_dd.fle
    $(CC) -w genddx ddfile=os_dd
    $(CC) $(CFLAGS) appl=os_ddx dotfle=$(TD)os_dd.fle

$(TD)osrt_dd.fle:  osrt_dd.lgc
    $(CC) $(CFLAGS) appl=osrt_dd dotfle=$(TD)osrt_dd.fle
    $(CC) -w genddx ddfile=osrt_dd
    $(CC) $(CFLAGS) appl=osrt_ddx dotfle=$(TD)os_dd.fle

$(TD)osdbutil.fle:  osdbutil.lgc
    $(CC) $(CFLAGS) appl=osdbutil dotfle=$(TD)osdbutil.fle
    $(CC) $(CFLAGS) appl=osdbutil dotfle=Y:\os\osdbutil.fle

$(TD)osdbfupd.fle:  osdbfupd.lgc
    $(CC) $(CFLAGS) appl=osdbfupd dotfle=$(TD)osdbfupd.fle

$(TD)os.fle:  os.lgc
    $(CC) $(CFLAGS) appl=os dotfle=$(TD)os.fle

$(TD)os_a_dd.fle:  os_a_dd.lgc
    $(CC) $(CFLAGS) appl=os_a_dd dotfle=$(TD)os_a_dd.fle

$(TD)os_flds.fle:  os_flds.lgc
    $(CC) $(CFLAGS) appl=os_flds dotfle=$(TD)os_flds.fle

$(TD)amgr.fle:  amgr.lgc
    $(CC) $(CFLAGS) appl=amgr dotfle=$(TD)amgr.fle

$(TD)amgr2.fle:  amgr2.lgc
    $(CC) $(CFLAGS) appl=amgr2 dotfle=$(TD)amgr2.fle

$(TD)l_amor.fle:  l_amor.lgc
    $(CC) $(CFLAGS) appl=l_amor dotfle=$(TD)l_amor.fle

$(TD)lamgr_arh.fle:  lamgr_arh.lgc
    $(CC) $(CFLAGS) appl=lamgr_arh dotfle=$(TD)lamgr_arh.fle

$(TD)osamgruparh.fle:  osamgruparh.lgc
    $(CC) $(CFLAGS) appl=osamgruparh dotfle=$(TD)osamgruparh.fle

$(TD)art.fle:  art.lgc
    $(CC) $(CFLAGS) appl=art dotfle=$(TD)art.fle

$(TD)invlist.fle:  invlist.lgc
    $(CC) $(CFLAGS) appl=invlist dotfle=$(TD)invlist.fle

$(TD)izvrknj.fle:  izvrknj.lgc
    $(CC) $(CFLAGS) appl=izvrknj dotfle=$(TD)izvrknj.fle

$(TD)lamgr.fle:  lamgr.lgc
    $(CC) $(CFLAGS) appl=lamgr dotfle=$(TD)lamgr.fle

$(TD)lart.fle:  lart.lgc
    $(CC) $(CFLAGS) appl=lart dotfle=$(TD)lart.fle

$(TD)likvid.fle:  likvid.lgc
    $(CC) $(CFLAGS) appl=likvid dotfle=$(TD)likvid.fle

$(TD)linvlist.fle:  linvlist.lgc
    $(CC) $(CFLAGS) appl=linvlist dotfle=$(TD)linvlist.fle

$(TD)linvrek.fle:  linvrek.lgc
    $(CC) $(CFLAGS) appl=linvrek dotfle=$(TD)linvrek.fle

$(TD)linvrepo.fle:  linvrepo.lgc
    $(CC) $(CFLAGS) appl=linvrepo dotfle=$(TD)linvrepo.fle

$(TD)lizvrknj.fle:  lizvrknj.lgc
    $(CC) $(CFLAGS) appl=lizvrknj dotfle=$(TD)lizvrknj.fle

$(TD)llikvid.fle:  llikvid.lgc
    $(CC) $(CFLAGS) appl=llikvid dotfle=$(TD)llikvid.fle

$(TD)lobramgr.fle:  lobramgr.lgc
    $(CC) $(CFLAGS) appl=lobramgr dotfle=$(TD)lobramgr.fle

$(TD)lobrkont.fle:  lobrkont.lgc
    $(CC) $(CFLAGS) appl=lobrkont dotfle=$(TD)lobrkont.fle

$(TD)lobrlik.fle:  lobrlik.lgc
    $(CC) $(CFLAGS) appl=lobrlik dotfle=$(TD)lobrlik.fle

$(TD)lobrlok.fle:  lobrlok.lgc
    $(CC) $(CFLAGS) appl=lobrlok dotfle=$(TD)lobrlok.fle

$(TD)lobrramg.fle:  lobrramg.lgc
    $(CC) $(CFLAGS) appl=lobrramg dotfle=$(TD)lobrramg.fle

$(TD)lobrrkon.fle:  lobrrkon.lgc
    $(CC) $(CFLAGS) appl=lobrrkon dotfle=$(TD)lobrrkon.fle

$(TD)lobrrlik.fle:  lobrrlik.lgc
    $(CC) $(CFLAGS) appl=lobrrlik dotfle=$(TD)lobrrlik.fle

$(TD)lobrrlok.fle:  lobrrlok.lgc
    $(CC) $(CFLAGS) appl=lobrrlok dotfle=$(TD)lobrrlok.fle

$(TD)lobrrorg.fle:  lobrrorg.lgc
    $(CC) $(CFLAGS) appl=lobrrorg dotfle=$(TD)lobrrorg.fle

$(TD)lobruamg.fle:  lobruamg.lgc
    $(CC) $(CFLAGS) appl=lobruamg dotfle=$(TD)lobruamg.fle

$(TD)lobrukon.fle:  lobrukon.lgc
    $(CC) $(CFLAGS) appl=lobrukon dotfle=$(TD)lobrukon.fle

$(TD)lobrulok.fle:  lobrulok.lgc
    $(CC) $(CFLAGS) appl=lobrulok dotfle=$(TD)lobrulok.fle

$(TD)losamgr.fle:  losamgr.lgc
    $(CC) $(CFLAGS) appl=losamgr dotfle=$(TD)losamgr.fle

$(TD)loskar.fle:  loskar.lgc
    $(CC) $(CFLAGS) appl=loskar dotfle=$(TD)loskar.fle

$(TD)loskont.fle:  loskont.lgc
    $(CC) $(CFLAGS) appl=loskont dotfle=$(TD)loskont.fle

$(TD)loslok.fle:  loslok.lgc
    $(CC) $(CFLAGS) appl=loslok dotfle=$(TD)loslok.fle

$(TD)losorgs.fle:  losorgs.lgc
    $(CC) $(CFLAGS) appl=losorgs dotfle=$(TD)losorgs.fle

$(TD)losppar.fle:  losppar.lgc
    $(CC) $(CFLAGS) appl=losppar dotfle=$(TD)losppar.fle

$(TD)losramgr.fle:  losramgr.lgc
    $(CC) $(CFLAGS) appl=losramgr dotfle=$(TD)losramgr.fle

$(TD)losrkont.fle:  losrkont.lgc
    $(CC) $(CFLAGS) appl=losrkont dotfle=$(TD)losrkont.fle

$(TD)losrlok.fle:  losrlok.lgc
    $(CC) $(CFLAGS) appl=losrlok dotfle=$(TD)losrlok.fle

$(TD)losrppar.fle:  losrppar.lgc
    $(CC) $(CFLAGS) appl=losrppar dotfle=$(TD)losrppar.fle

$(TD)losuamgr.fle:  losuamgr.lgc
    $(CC) $(CFLAGS) appl=losuamgr dotfle=$(TD)losuamgr.fle

$(TD)losukar.fle:  losukar.lgc
    $(CC) $(CFLAGS) appl=losukar dotfle=$(TD)losukar.fle

$(TD)losukont.fle:  losukont.lgc
    $(CC) $(CFLAGS) appl=losukont dotfle=$(TD)losukont.fle

$(TD)losuorgs.fle:  losuorgs.lgc
    $(CC) $(CFLAGS) appl=losuorgs dotfle=$(TD)losuorgs.fle

$(TD)losuppar.fle:  losuppar.lgc
    $(CC) $(CFLAGS) appl=losuppar dotfle=$(TD)losuppar.fle

$(TD)lresk.fle:  lresk.lgc
    $(CC) $(CFLAGS) appl=lresk dotfle=$(TD)lresk.fle

$(TD)lsikar.fle:  lsikar.lgc
    $(CC) $(CFLAGS) appl=lsikar dotfle=$(TD)lsikar.fle

$(TD)lsikont.fle:  lsikont.lgc
    $(CC) $(CFLAGS) appl=lsikont dotfle=$(TD)lsikont.fle

$(TD)lsiorgs.fle:  lsiorgs.lgc
    $(CC) $(CFLAGS) appl=lsiorgs dotfle=$(TD)lsiorgs.fle

$(TD)lvrpr.fle:  lvrpr.lgc
    $(CC) $(CFLAGS) appl=lvrpr dotfle=$(TD)lvrpr.fle

$(TD)l_visman.fle:  l_visman.lgc
    $(CC) $(CFLAGS) appl=l_visman dotfle=$(TD)l_visman.fle

$(TD)manjak.fle:  manjak.lgc
    $(CC) $(CFLAGS) appl=manjak dotfle=$(TD)manjak.fle

$(TD)os_obrac.fle:  os_obrac.lgc
    $(CC) $(CFLAGS) appl=os_obrac dotfle=$(TD)os_obrac.fle

$(TD)obrlik.fle:  obrlik.lgc
    $(CC) $(CFLAGS) appl=obrlik dotfle=$(TD)obrlik.fle

$(TD)oskar.fle:  oskar.lgc
    $(CC) $(CFLAGS) appl=oskar dotfle=$(TD)oskar.fle

$(TD)oskt.fle:  oskt.lgc
    $(CC) $(CFLAGS) appl=oskt dotfle=$(TD)oskt.fle

$(TD)osos.fle:  osos.lgc
    $(CC) $(CFLAGS) appl=osos dotfle=$(TD)osos.fle

$(TD)osprom.fle:  osprom.lgc
    $(CC) $(CFLAGS) appl=osprom dotfle=$(TD)osprom.fle

$(TD)osulaz.fle:  osulaz.lgc
    $(CC) $(CFLAGS) appl=osulaz dotfle=$(TD)osulaz.fle

$(TD)pocsta.fle:  pocsta.lgc
    $(CC) $(CFLAGS) appl=pocsta dotfle=$(TD)pocsta.fle

$(TD)resk.fle:  resk.lgc
    $(CC) $(CFLAGS) appl=resk dotfle=$(TD)resk.fle

$(TD)rinvlist.fle:  rinvlist.lgc
    $(CC) $(CFLAGS) appl=rinvlist dotfle=$(TD)rinvlist.fle

$(TD)rinvrek.fle:  rinvrek.lgc
    $(CC) $(CFLAGS) appl=rinvrek dotfle=$(TD)rinvrek.fle

$(TD)rinvrepo.fle:  rinvrepo.lgc
    $(CC) $(CFLAGS) appl=rinvrepo dotfle=$(TD)rinvrepo.fle

$(TD)toascii.fle:  toascii.lgc
    $(CC) $(CFLAGS) appl=toascii dotfle=$(TD)toascii.fle

$(TD)upariv.fle:  upariv.lgc
    $(CC) $(CFLAGS) appl=upariv dotfle=$(TD)upariv.fle

$(TD)visak.fle:  visak.lgc
    $(CC) $(CFLAGS) appl=visak dotfle=$(TD)visak.fle

$(TD)visman.fle:  visman.lgc
    $(CC) $(CFLAGS) appl=visman dotfle=$(TD)visman.fle

$(TD)vismanpr.fle:  vismanpr.lgc
    $(CC) $(CFLAGS) appl=vismanpr dotfle=$(TD)vismanpr.fle

$(TD)vrpr.fle:  vrpr.lgc
    $(CC) $(CFLAGS) appl=vrpr dotfle=$(TD)vrpr.fle

$(TD)os_zim.fle:  os_zim.lgc
    $(CC) $(CFLAGS) appl=os_zim dotfle=$(TD)os_zim.fle

$(TD)os_pren.fle:  os_pren.lgc
    $(CC) $(CFLAGS) appl=os_pren dotfle=$(TD)os_pren.fle

$(TD)os_pren2.fle:  os_pren2.lgc
    $(CC) $(CFLAGS) appl=os_pren2 dotfle=$(TD)os_pren2.fle

$(TD)ospasswd.fle:  ospasswd.lgc
    $(CC) $(CFLAGS) appl=ospasswd dotfle=$(TD)ospasswd.fle

$(TD)osdelvm.fle:  osdelvm.lgc
    $(CC) $(CFLAGS) appl=osdelvm dotfle=$(TD)osdelvm.fle

$(TD)visakupd.fle:  visakupd.lgc
    $(CC) $(CFLAGS) appl=visakupd dotfle=$(TD)visakupd.fle

$(TD)losulok.fle:  losulok.lgc
    $(CC) $(CFLAGS) appl=losulok dotfle=$(TD)losulok.fle

$(TD)oscorect.fle:  oscorect.lgc
    $(CC) $(CFLAGS) appl=oscorect dotfle=$(TD)oscorect.fle

$(TD)oschorg.fle:  oschorg.lgc
    $(CC) $(CFLAGS) appl=oschorg dotfle=$(TD)oschorg.fle

$(TD)loschorg.fle:  loschorg.lgc
    $(CC) $(CFLAGS) appl=loschorg dotfle=$(TD)loschorg.fle

$(TD)predamor.fle:  predamor.lgc
    $(CC) $(CFLAGS) appl=predamor dotfle=$(TD)predamor.fle

$(TD)lpredamo.fle:  lpredamo.lgc
    $(CC) $(CFLAGS) appl=lpredamo dotfle=$(TD)lpredamo.fle

$(TD)lpreamot.fle:  lpreamot.lgc
    $(CC) $(CFLAGS) appl=lpreamot dotfle=$(TD)lpreamot.fle

$(TD)lpredkon.fle:  lpredkon.lgc
    $(CC) $(CFLAGS) appl=lpredkon dotfle=$(TD)lpredkon.fle

$(TD)lprekont.fle:  lprekont.lgc
    $(CC) $(CFLAGS) appl=lprekont dotfle=$(TD)lprekont.fle

$(TD)losart.fle:  losart.lgc
    $(CC) $(CFLAGS) appl=losart dotfle=$(TD)losart.fle

$(TD)losuart.fle:  losuart.lgc
    $(CC) $(CFLAGS) appl=losuart dotfle=$(TD)losuart.fle

$(TD)losrart.fle:  losrart.lgc
    $(CC) $(CFLAGS) appl=losrart dotfle=$(TD)losrart.fle

$(TD)lrbaramg.fle:  lrbaramg.lgc
    $(CC) $(CFLAGS) appl=lrbaramg dotfle=$(TD)lrbaramg.fle

$(TD)lrbauamg.fle:  lrbauamg.lgc
    $(CC) $(CFLAGS) appl=lrbauamg dotfle=$(TD)lrbauamg.fle

$(TD)lrbarkon.fle:  lrbarkon.lgc
    $(CC) $(CFLAGS) appl=lrbarkon dotfle=$(TD)lrbarkon.fle

$(TD)lrbaukon.fle:  lrbaukon.lgc
    $(CC) $(CFLAGS) appl=lrbaukon dotfle=$(TD)lrbaukon.fle

$(TD)osrt.fle:  osrt.lgc
    $(CC) $(CFLAGS) appl=osrt dotfle=$(TD)osrt.fle

$(TD)invrlist.fle:  invrlist.lgc
    $(CC) $(CFLAGS) appl=invrlist dotfle=$(TD)invrlist.fle

$(TD)ososadd.fle:  ososadd.lgc
    $(CC) $(CFLAGS) appl=ososadd dotfle=$(TD)ososadd.fle

$(TD)ramorek.fle:  ramorek.lgc
    $(CC) $(CFLAGS) appl=ramorek dotfle=$(TD)ramorek.fle

$(TD)ramorepo.fle:  ramorepo.lgc
    $(CC) $(CFLAGS) appl=ramorepo dotfle=$(TD)ramorepo.fle
    
$(TD)lchorgal.fle:  lchorgal.lgc
    $(CC) $(CFLAGS) appl=lchorgal dotfle=$(TD)lchorgal.fle
    
$(TD)osaktiv.fle:  osaktiv.lgc
    $(CC) $(CFLAGS) appl=osaktiv dotfle=$(TD)osaktiv.fle
    
$(TD)ospredrac.fle:  ospredrac.lgc
    $(CC) $(CFLAGS) appl=ospredrac dotfle=$(TD)ospredrac.fle
    
$(TD)lrbapred.fle:  lrbapred.lgc
    $(CC) $(CFLAGS) appl=lrbapred dotfle=$(TD)lrbapred.fle
    
$(TD)lrbapred_mj.fle:  lrbapred_mj.lgc
    $(CC) $(CFLAGS) appl=lrbapred_mj dotfle=$(TD)lrbapred_mj.fle

$(TD)losaktiv.fle:  losaktiv.lgc
    $(CC) $(CFLAGS) appl=losaktiv dotfle=$(TD)losaktiv.fle
    
$(TD)laktival.fle:  laktival.lgc
    $(CC) $(CFLAGS) appl=laktival dotfle=$(TD)laktival.fle
    
$(TD)os_ponob.fle:  os_ponob.lgc
    $(CC) $(CFLAGS) appl=os_ponob dotfle=$(TD)os_ponob.fle
    
$(TD)osrt_1.fle:  osrt_1.lgc
    $(CC) $(CFLAGS) appl=osrt_1 dotfle=$(TD)osrt_1.fle

$(TD)losobj.fle:  losobj.lgc
    $(CC) $(CFLAGS) appl=losobj dotfle=$(TD)losobj.fle

$(TD)losuobj.fle:  losuobj.lgc
    $(CC) $(CFLAGS) appl=losuobj dotfle=$(TD)losuobj.fle

$(TD)losrobj.fle:  losrobj.lgc
    $(CC) $(CFLAGS) appl=losrobj dotfle=$(TD)losrobj.fle

$(TD)lizknjob.fle:  lizknjob.lgc
    $(CC) $(CFLAGS) appl=lizknjob dotfle=$(TD)lizknjob.fle

$(TD)os_prim.fle:  os_prim.lgc
    $(CC) $(CFLAGS) appl=os_prim dotfle=$(TD)os_prim.fle

$(TD)os_prims.fle:  os_prims.lgc
    $(CC) $(CFLAGS) appl=os_prims dotfle=$(TD)os_prims.fle

$(TD)os_sklad.fle:  os_sklad.lgc
    $(CC) $(CFLAGS) appl=os_sklad dotfle=$(TD)os_sklad.fle

$(TD)os_nal.fle:  os_nal.lgc
    $(CC) $(CFLAGS) appl=os_nal dotfle=$(TD)os_nal.fle

$(TD)los_nal.fle:  los_nal.lgc
    $(CC) $(CFLAGS) appl=los_nal dotfle=$(TD)los_nal.fle

$(TD)os_tab.fle:  os_tab.lgc
    $(CC) $(CFLAGS) appl=os_tab dotfle=$(TD)os_tab.fle

$(TD)os_kalk.fle:  os_kalk.lgc
    $(CC) $(CFLAGS) appl=os_kalk dotfle=$(TD)os_kalk.fle

$(TD)os_kalks.fle:  os_kalks.lgc
    $(CC) $(CFLAGS) appl=os_kalks dotfle=$(TD)os_kalks.fle

$(TD)lizknorg.fle:  lizknorg.lgc
    $(CC) $(CFLAGS) appl=lizknorg dotfle=$(TD)lizknorg.fle

$(TD)losrevobr.fle:  losrevobr.lgc
    $(CC) $(CFLAGS) appl=losrevobr dotfle=$(TD)losrevobr.fle

$(TD)ispis_zbroja.fle:  ispis_zbroja.lgc
    $(CC) $(CFLAGS) appl=ispis_zbroja dotfle=$(TD)ispis_zbroja.fle

$(TD)os_rba_kalk.fle:  os_rba_kalk.lgc
    $(CC) $(CFLAGS) appl=os_rba_kalk dotfle=$(TD)os_rba_kalk.fle

$(TD)los_rba_kalk.fle:  los_rba_kalk.lgc
    $(CC) $(CFLAGS) appl=los_rba_kalk dotfle=$(TD)los_rba_kalk.fle

$(TD)os_mnajag.fle:  os_mnajag.lgc
    $(CC) $(CFLAGS) appl=os_mnajag dotfle=$(TD)os_mnajag.fle

$(TD)os_mnajam.fle:  os_mnajam.lgc
    $(CC) $(CFLAGS) appl=os_mnajam dotfle=$(TD)os_mnajam.fle

$(TD)os_mnajfak.fle:  os_mnajfak.lgc
    $(CC) $(CFLAGS) appl=os_mnajfak dotfle=$(TD)os_mnajfak.fle

$(TD)volkos2.fle:  volkos2.lgc
    $(CC) $(CFLAGS) appl=volkos2 dotfle=$(TD)volkos2.fle

$(TD)los_mnaj.fle:  los_mnaj.lgc
    $(CC) $(CFLAGS) appl=los_mnaj dotfle=$(TD)los_mnaj.fle

$(TD)los_mnaj2.fle:  los_mnaj2.lgc
    $(CC) $(CFLAGS) appl=los_mnaj2 dotfle=$(TD)los_mnaj2.fle

$(TD)los_mnajag.fle:  los_mnajag.lgc
    $(CC) $(CFLAGS) appl=los_mnajag dotfle=$(TD)los_mnajag.fle

$(TD)osmnprfak.fle:  osmnprfak.lgc
    $(CC) $(CFLAGS) appl=osmnprfak dotfle=$(TD)osmnprfak.fle

$(TD)losmnrek.fle:  losmnrek.lgc
    $(CC) $(CFLAGS) appl=losmnrek dotfle=$(TD)losmnrek.fle

$(TD)losmnpojed.fle:  losmnpojed.lgc
    $(CC) $(CFLAGS) appl=losmnpojed dotfle=$(TD)losmnpojed.fle

$(TD)los_mnzap.fle:  los_mnzap.lgc
    $(CC) $(CFLAGS) appl=los_mnzap dotfle=$(TD)los_mnzap.fle

$(TD)os_mnzap.fle:  os_mnzap.lgc
    $(CC) $(CFLAGS) appl=os_mnzap dotfle=$(TD)os_mnzap.fle

$(TD)losloksi.fle:  losloksi.lgc
    $(CC) $(CFLAGS) appl=losloksi dotfle=$(TD)losloksi.fle

$(TD)os_idx.fle:  os_idx.lgc
    $(CC) $(CFLAGS) appl=os_idx dotfle=$(TD)os_idx.fle

$(TD)os_start.fle:  os_start.lgc
    $(CC) $(CFLAGS) appl=os_start dotfle=$(TD)os_start.fle

$(TD)lchorg_svi.fle:  lchorg_svi.lgc
    $(CC) $(CFLAGS) appl=lchorg_svi dotfle=$(TD)lchorg_svi.fle

$(TD)ossredpov.fle:  ossredpov.lgc
    $(CC) $(CFLAGS) appl=ossredpov dotfle=$(TD)ossredpov.fle

$(TD)os_rev.fle:  os_rev.lgc
    $(CC) $(CFLAGS) appl=os_rev dotfle=$(TD)os_rev.fle
    COPY os_rev.lgc $(YTD)os_rev.lgc

